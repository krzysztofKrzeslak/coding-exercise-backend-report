# Coding exercise realization report

Because during realization of this exercise we encountered some problems, we've decided to give some feedback about that.Generally our problems was with our development enviroment, because of requriment of using cutting edge symfony4. In such cases providing some type of pre set enviroment(for ex. using docker) is generally good practice. But this problems we treated as challenge and get this working, here i describe what we needed to do acomplish that.

So when i started realization of this exercise i had virtual machine running debian Jessie with lamp stack installed, which included php in version 7.0.24	

After cloning exercise repo and trying to run composer install:


![error1][err1]

this was easy, needed to run: 	
```/opt/bitnami/php/bin/composer.phar self-update```	

Next we we run ```composer install``` (in repo directory of course)	and we get:


 ![error2][err2]
 
so to resolve this we need php 7.1, i followed this article to install it: http://blog.programster.org/debian-8-install-php-7-1		
and then we need to make php7.1 default version of used php, so on debian i did this with this commands:	
```
$ sudo update-alternatives --set php /usr/bin/php7.1
$ sudo update-alternatives --set phar /usr/bin/phar7.1
$ sudo update-alternatives --set phar.phar /usr/bin/phar.phar7.1
```


but after this problem still occurs it looks like composer still want to use older version of php:	

 ![error3][err3]
 
to overcome this i run composer with this command:	
``` php7.1 /opt/bitnami/php/bin/composer.phar install```

After this we get error about missing xml library:	

 ![error4][err4]	

so to install it: ```sudo apt-get install php-xml```	
when we run again composer installation it goes well until we get:	

 ![error5][err5]
 
so we need to run ```touch .env```

and finnaly run last time composer installation, aaand voila:	

 ![ready][ok]

because symfony4 don't have builtin development server(can be installed as dependency) as version 3 was, we used php built-in server, to do this	
```
cd public
php -S [server_ip]:8000
```	
on my machine(local Vm) i also needed to open port 8000 with:		
```
sudo iptables -A INPUT -p tcp --dport 8000 --jump ACCEPT
sudo iptables-save
```
[err1]: err1.png
[err2]: err2.png
[err3]: err3.png
[err4]: err4.png
[err5]: err5.png
[ok1]: ok1.png

